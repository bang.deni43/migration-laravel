<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf 
        <Label>First Name :</Label> <br> <br>
        <input type="text" name="nama_depan"> <br> <br>
        <label>Last Name :</label> <br> <br>
        <input type="text" name="nama_belakang"> <br> <br>
        <Label> Gender :</Label> <br> <br>
        <input type="radio" name="gn" value="Male"> Male <br>
        <input type="radio" name="gn" value="Female"> Female <br>
        <input type="radio" name="gn" value="Other"> Other <br> <br>
        <label> Nationality :</label> <br> <br>
        <select name="nt">
            <option value="1">Indonesia</option>
            <option value="1">Amerika</option>
            <option value="1">Inggris</option>
        </select> <br> <br>
        <label> Language Spoken :</label> <br> <br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br> <br>
        <label> Bio</label> <br> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>